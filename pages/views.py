from django.shortcuts import render, redirect
import stravalib
import os
from dotenv import load_dotenv
load_dotenv()


def home_view(request):
    client = stravalib.client.Client()
    client_id = os.getenv("strava_client_id")
    authorization_url = client.authorization_url(
        client_id=client_id, 
        redirect_uri="http://localhost:8000/authenticate",
        scope="activity:write"
        )
    context = {
        "authorization_url": authorization_url
    }
    return render(request, "home.html", context)