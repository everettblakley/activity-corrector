from django.urls import path
from .views import (
    authentication_view,
    login_view
    )

app_name = 'authentication'
urlpatterns = [
    path('', authentication_view, name='authentication'),
    path('login/', login_view, name='authentication-login'),
]