from django.shortcuts import render, redirect
import stravalib
import os
from dotenv import load_dotenv
load_dotenv()

def authentication_view(request):
    try:
        if request.session['strava_access_token'] == None:
            code = request.GET.get('code')
            client = stravalib.client.Client()
            client_id = os.getenv("strava_client_id")
            client_secret = os.getenv("strava_client_secret")
            access_token = client.exchange_code_for_token(client_id=client_id, client_secret=client_secret, code=code)
            request.session['strava_access_token'] = access_token
        return redirect("/activities")
    except KeyError:
        return redirect("/")
    return render(request, "authentication/authenticate.html")

def login_view(request):
    return render(request, "authentication/login.html")