from django.shortcuts import render, redirect
import stravalib
import os
from dotenv import load_dotenv
load_dotenv()

def activities_list_view(request):
    access_token = request.session['strava_access_token']
    if access_token == None:
        return redirect("/")
    client = stravalib.client.Client(access_token=access_token['access_token'])
    activities = client.get_activities()
    context = {
        "object": activities
    }
    return render(request, "activities.html", context)
