from django.urls import path
from .views import (
    activities_list_view
    )

app_name = 'activities'
urlpatterns = [
    path('', activities_list_view, name='activities-list'),
]